# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Jeff Bailes <thepizzaking@gmail.com>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-30 00:54+0100\n"
"PO-Revision-Date: 2022-01-29 23:54+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/xfce/xfce-panel-plugins/language/en_GB/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/cpu.c:63
msgid "File /proc/stat not found!"
msgstr "File /proc/stat not found!"

#: ../panel-plugin/systemload.c:217
#, c-format
msgid "System Load: %ld%%"
msgstr "System Load: %ld%%"

#: ../panel-plugin/systemload.c:225
#, c-format
msgid "Memory: %ldMB of %ldMB used"
msgstr "Memory: %ldMB of %ldMB used"

#: ../panel-plugin/systemload.c:233
#, c-format
msgid "Network: %ld Mbit/s"
msgstr ""

#: ../panel-plugin/systemload.c:243
#, c-format
msgid "Swap: %ldMB of %ldMB used"
msgstr "Swap: %ldMB of %ldMB used"

#: ../panel-plugin/systemload.c:246
#, c-format
msgid "No swap"
msgstr "No swap"

#: ../panel-plugin/systemload.c:261
#, c-format
msgid "%dd"
msgstr ""

#: ../panel-plugin/systemload.c:262
#, c-format
msgid "%dh"
msgstr ""

#: ../panel-plugin/systemload.c:263
#, c-format
msgid "%dm"
msgstr ""

#: ../panel-plugin/systemload.c:265
#, c-format
msgid "%d day"
msgid_plural "%d days"
msgstr[0] "%d day"
msgstr[1] "%d days"

#: ../panel-plugin/systemload.c:266
#, c-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] ""
msgstr[1] ""

#: ../panel-plugin/systemload.c:267
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] ""
msgstr[1] ""

#: ../panel-plugin/systemload.c:274
#, c-format
msgid "Uptime: %s, %s, %s"
msgstr ""

#: ../panel-plugin/systemload.c:750
msgid "Label:"
msgstr "Label:"

#: ../panel-plugin/systemload.c:759
msgid "Leave empty to disable the label"
msgstr ""

#: ../panel-plugin/systemload.c:800
msgid "CPU monitor"
msgstr "CPU monitor"

#: ../panel-plugin/systemload.c:801
msgid "Memory monitor"
msgstr "Memory monitor"

#: ../panel-plugin/systemload.c:802
msgid "Network monitor"
msgstr ""

#: ../panel-plugin/systemload.c:803
msgid "Swap monitor"
msgstr "Swap monitor"

#: ../panel-plugin/systemload.c:804
msgid "Uptime monitor"
msgstr "Uptime monitor"

#: ../panel-plugin/systemload.c:815 ../panel-plugin/systemload.desktop.in.h:1
msgid "System Load Monitor"
msgstr "System Load Monitor"

#: ../panel-plugin/systemload.c:818
msgid "_Close"
msgstr "_Close"

#: ../panel-plugin/systemload.c:819
msgid "_Help"
msgstr ""

#: ../panel-plugin/systemload.c:837
msgid "<b>General</b>"
msgstr ""

#: ../panel-plugin/systemload.c:856
msgid "Update interval:"
msgstr "Update interval:"

#: ../panel-plugin/systemload.c:862
msgid ""
"Update interval when running on battery (uses regular update interval if set"
" to zero)"
msgstr ""

#: ../panel-plugin/systemload.c:873
msgid "Power-saving interval:"
msgstr "Power-saving interval:"

#: ../panel-plugin/systemload.c:880
msgid "Launched when clicking on the plugin"
msgstr ""

#: ../panel-plugin/systemload.c:887
msgid "System monitor:"
msgstr ""

#: ../panel-plugin/systemload.c:925 ../panel-plugin/systemload.desktop.in.h:2
msgid "Monitor CPU load, swap usage and memory footprint"
msgstr "Monitor CPU load, swap usage and memory footprint"

#: ../panel-plugin/systemload.c:927
msgid "Copyright (c) 2003-2022\n"
msgstr ""

#: ../panel-plugin/uptime.c:52
msgid "File /proc/uptime not found!"
msgstr "File /proc/uptime not found!"
